﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Forum.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Topic> Topics { get; set; }
    }
}
