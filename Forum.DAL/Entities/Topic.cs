﻿using System;

namespace Forum.DAL.Entities
{
    public class Topic : IEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Headline { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
    }
}
