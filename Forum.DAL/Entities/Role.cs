﻿using Microsoft.AspNetCore.Identity;

namespace Forum.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
