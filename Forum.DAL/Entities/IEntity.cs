﻿namespace Forum.DAL.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
