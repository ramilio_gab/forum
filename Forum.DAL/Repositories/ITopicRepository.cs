﻿using System;
using System.Collections.Generic;
using System.Text;
using Forum.DAL.Entities;
using Forum.DAL.Repositories.Contracts;

namespace Forum.DAL.Repositories
{
    public interface ITopicRepository : IRepository<Topic>

    {
        IEnumerable<Topic> GetAllWithAuthors();
    }
}
