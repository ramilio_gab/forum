﻿using System.Collections.Generic;
using System.Linq;
using Forum.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class TopicRepository : Repository<Topic>, ITopicRepository
    {
        public TopicRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Topics;
        }

        public IEnumerable<Topic> GetAllWithAuthors()
        {
            return entities
                .Include(e => e.Author)
                .ToList();

        }
    }
}
