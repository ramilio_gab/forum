﻿using Forum.DAL.Entities;
using Forum.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>

    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;
        public DbSet<Topic> Topics { get; set; }

        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.TopiConfiguration.ProvideConfigurationAction());

        }

    }
}