﻿using System;
using System.Collections.Generic;
using System.Text;
using Forum.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.DAL.EntitiesConfiguration
{
    public class TopicConfiguration : BaseEntityConfiguration<Topic>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Topic> builder)
        {
            builder
                .Property(b => b.Headline)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(b => b.DateCreated)
                .IsRequired();

        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Topic> builder)
        {
            builder
                .HasOne(b => b.Author)
                .WithMany(b => b.Topics)
                .HasForeignKey(b => b.AuthorId)
                .IsRequired();
        }
    }
}
