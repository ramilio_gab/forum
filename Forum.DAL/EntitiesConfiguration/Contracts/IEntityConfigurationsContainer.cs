﻿using Forum.DAL.Entities;

namespace Forum.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Topic> TopiConfiguration { get; }
    }
}