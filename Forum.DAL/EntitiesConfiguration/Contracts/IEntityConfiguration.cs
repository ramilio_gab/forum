﻿using System;
using Forum.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
