﻿using Forum.DAL.Entities;


namespace Forum.DAL.EntitiesConfiguration.Contracts
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Topic> TopiConfiguration { get; }
        public EntityConfigurationsContainer()
        {
            TopiConfiguration = new TopicConfiguration();
        }

        
    }
}
