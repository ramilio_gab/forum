﻿using System.Collections.Generic;

namespace Forum.Models
{
    public class TopicIndexModel
    {
        public List<TopicModel> Topics { get; set; }
        public int? Page { get; set; }
        public PagingModel PagingModel { get; set; }
        
    }
}
