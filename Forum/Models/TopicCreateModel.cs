﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class TopicCreateModel
    {
        [Required]
        [Display(Name = "Название темы")]
        public string Headline { get; set; }
        [Required]
        [Display(Name = "Содержание")]
        public string Content { get; set; }
    }
}
