﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class TopicModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата создания")]
        public string DateCreated { get; set; }

        [Display(Name = "Автор темы")]
        public string AuthorName { get; set; }

        [Display(Name = "Название темы")]
        public string Headline { get; set; }

        [Display(Name = "Содержание")]
        
        public string ContentPreview { get; set; }
    }
}
