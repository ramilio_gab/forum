﻿using System.Collections.Generic;
using Forum.Models;

namespace Forum.Services.Contracts
{
    public interface ITopicService
    {
        List<TopicModel> GetAllTopics(TopicIndexModel model);
        void CreateTopic(TopicCreateModel model, int currentUserId);
    }
}
