﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Forum.DAL.Entities;
using Forum.DAL.Repositories.Contracts;
using Forum.Models;
using Forum.Services.Contracts;

namespace Forum.Services
{
    public class TopicService : ITopicService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
    

        public TopicService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<TopicModel> GetAllTopics(TopicIndexModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topics = unitOfWork.Topics.GetAllWithAuthors();

                int pageSIze = 2;
                int count = topics.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                topics = topics.Skip((page - 1) * pageSIze).Take(pageSIze);
                model.PagingModel = new PagingModel(count, page, pageSIze);
                model.Page = page;

                var models = Mapper.Map<List<TopicModel>>(topics.ToList());
                
                return models;
            }
        }


        
        public void CreateTopic(TopicCreateModel model, int currentUserId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topic = Mapper.Map<Topic>(model);
                topic.AuthorId = currentUserId;
                
                unitOfWork.Topics.Create(topic);
            }
        }
    }
}
