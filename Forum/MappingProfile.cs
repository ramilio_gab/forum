﻿using System;
using AutoMapper;
using Forum.DAL.Entities;
using Forum.Models;

namespace Forum
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateTopicToTopicModelMap();
            CreateTopicCreateModelToTopic();
        }

        public void CreateTopicToTopicModelMap()
        {
            CreateMap<Topic, TopicModel>()
                .ForMember(target => target.DateCreated,
                    src => src.MapFrom(p => p.DateCreated.ToString("D")))
                .ForMember(target => target.AuthorName,
                    src => src.MapFrom(p => p.Author.UserName))
                .ForMember(target => target.ContentPreview,
                    src => src.MapFrom(p => p.Content.Substring(0, 20)));
        }

        public void CreateTopicCreateModelToTopic()
        {
            CreateMap<TopicCreateModel, Topic>()
                .ForMember(target => target.DateCreated,
                    src => src.MapFrom(p => DateTime.Now));
        }
    }
}
