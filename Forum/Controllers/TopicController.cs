﻿using System;
using System.Threading.Tasks;
using Forum.DAL.Entities;
using Forum.Models;
using Forum.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class TopicController : Controller
    {
        private readonly ITopicService _topicService;
        private readonly UserManager<User> _userManager;

        public TopicController(ITopicService topicService, UserManager<User> userManager)
        {
            _topicService = topicService;
            _userManager = userManager;
        }

        public IActionResult Index(TopicIndexModel model)
        {
            try
            {
                var topicModels = _topicService.GetAllTopics(model);
                model.Topics = topicModels;
                
                return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]

        public async Task<IActionResult> CreateTopic(TopicCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _topicService.CreateTopic(model, currentUser.Id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

}
